# Diaper Designs

Diapers come in different designs. This article destinguishes between the shape(pull-up, tape style) and the visuals style.

## General shape

### Disposable taped diapers
Usually diapers are put on by applying tapes. These tapes can either use an adhesive or an hook-and-loop fastener (velcro). 
Hook-and-loop tapes have fine texture of tiny hook shaped extrusions, these hook interlock with the cloth exteriour of the diapers. Some diapers will only have a cloth texture on the [landing zone](landing_zone.md). B
Adhesive tapes require a smooth foil surface to stick to. Some Diapers have an aditional thicker layer of plastic foil on the landing zone to avoid the the plastic ripping. 


### Disposable absorbent underwear ("pullups")

This kind of diapers are put on like regular underwear, they are pulled up over the legs and are hold in place by the elastic waist band. 
Many manufacturers try to avoid calling them diapers to avoid the association with baby diapers or confusion with tapeed diapers. 
Pull-up style diapers usually hold less liquid and are not recommended for messing.
The question whether or not pull-up are considered "real" diapers is source of controversy in the ABDL community. 
Due to theire availability, many ABDLs made theire first experiences with these style of diapers. 

Also called "training pants", "nappy pants", "trainer pants", "Pull-Ups", "GoodNites", "DryNites", "Depends", "Pyjama pants"
See also [Wikipedia: Training pants](https://en.wikipedia.org/wiki/Training_pants)

## Reuseable diapers

## Reuseable absorbant underwear

## Plastic pants (reusable)
